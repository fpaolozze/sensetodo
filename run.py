import api
from app import APP
from config import Config
from db import DB
import views

DB.create_all()
APP.register_blueprint(api.tasks.bp)
APP.register_blueprint(views.tasks.bp)

if __name__ == '__main__':
    APP.run(host='0.0.0.0',port=Config['host_port'])
