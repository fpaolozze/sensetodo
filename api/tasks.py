from flask import Blueprint, request, Response
from models.tasks import Tasks
from simplejson import dumps, loads, JSONDecodeError

__all__ = ['bp']

bp = Blueprint('api.tasks', __name__, url_prefix='/api/tasks')


def _stream_json(tasks_list):
    try:
        yield '[' + dumps(tasks_list[0].to_dict())
        for task in tasks_list[1:]:
            yield ',' + dumps(task.to_dict())
        yield ']'
    except IndexError:
        yield '[]'
    except Exception:
        raise


def __task_create():
    print request.data
    try:
        return Response(
            dumps({
                'id': Tasks.create(**loads(request.data))
            }),
            status=201,
            mimetype='application/json'
        )
    except (KeyError, JSONDecodeError, TypeError) as e:
        print e
        return Response('', status=400)
    except Exception:
        raise


def __task_delete(task_id):
    Tasks.delete(task_id)
    return Response('', status=200)


def __task_get(task_id):
    try:
        task = Tasks.get(task_id)
        assert task
    except AssertionError:
        return Response('', status=404)
    except Exception:
        raise
    else:
        return (
            dumps(task.to_dict()),
            200,
            {'Content-Type': 'application/json'}
        )


def __tasks_list():
    return Response(
        _stream_json(Tasks.list()),
        mimetype='application/json'
    )


__tasks = {
    'GET': __tasks_list,
    'POST': __task_create
}

__task_row = {
    'DELETE': __task_delete,
    'GET': __task_get
}


@bp.route('/', methods=['GET', 'POST'])
def tasks():
    resp = __tasks[request.method]()
    resp.headers['Access-Control-Allow-Origin'] = 'http://localhost:3449'
    resp.headers['Access-Control-Allow-Credentials'] = 'true'
    return resp


@bp.route('/completed')
def completed_tasks():
    resp = Response(
        _stream_json(Tasks.list_completed()),
        mimetype='application/json'
    )
    resp.headers['Access-Control-Allow-Origin'] = 'http://localhost:3449'
    resp.headers['Access-Control-Allow-Credentials'] = 'true'
    return resp


@bp.route('/pending')
def pending_tasks():
    resp = Response(
        _stream_json(Tasks.list_pending()),
        mimetype='application/json'
    )
    resp.headers['Access-Control-Allow-Origin'] = 'http://localhost:3449'
    resp.headers['Access-Control-Allow-Credentials'] = 'true'
    return resp


@bp.route('/<task_id>', methods=['DELETE', 'GET'])
def task(task_id):
    try:
        resp = __task_row[request.method](int(task_id))
    except ValueError:
        resp = Response('', status=400)
    except Exception:
        raise

    resp.headers['Access-Control-Allow-Origin'] = 'http://localhost:3449'
    resp.headers['Access-Control-Allow-Credentials'] = 'true'
    return resp


@bp.route('/<task_id>/complete', methods=['PUT'])
def complete_task(task_id):
    if Tasks.complete(task_id):
        return ('', 204)
    return ('', 409)
