from app import APP
from config import Config
from flask_sqlalchemy import SQLAlchemy

__all__ = ['DB']

APP.config['SQLALCHEMY_DATABASE_URI'] = Config['psql_uri']
APP.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
DB = SQLAlchemy(APP)
