from datetime import datetime
from db import DB

__all__ = ['Tasks']

class Tasks(DB.Model):
    id = DB.Column(DB.Integer, primary_key=True)
    text = DB.Column(DB.String(255), nullable=False)
    created = DB.Column(DB.DateTime(), nullable=False, default=datetime.utcnow)
    completed = DB.Column(DB.DateTime())

    @staticmethod
    def complete(task_id):
        task = Tasks.query.get(task_id)
        if task.completed:
            return False
        task.completed = datetime.utcnow()
        DB.session.add(task)
        DB.session.commit()
        return True

    @staticmethod
    def create(text):
        task = Tasks(text=text)
        DB.session.add(task)
        DB.session.commit()
        return task.id

    @staticmethod
    def delete(task_id):
        Tasks.query.filter_by(id=task_id).delete()
        DB.session.commit()
        return True

    @staticmethod
    def get(task_id):
        return Tasks.query.get(task_id)

    @staticmethod
    def list():
        return Tasks.query.all()

    @staticmethod
    def list_completed():
        return Tasks.query.filter(Tasks.completed.isnot(None)).all()

    @staticmethod
    def list_pending():
        return Tasks.query.filter(Tasks.completed.is_(None)).all()

    def to_dict(self):
        return {
            'id':self.id,
            'text':self.text,
            'created':self.created.isoformat(),
            'completed':self.completed and self.completed.isoformat()
        }
