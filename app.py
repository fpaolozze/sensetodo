from flask import Flask

__all__ = ['APP']

APP = Flask(__name__, static_folder='public', static_url_path='')
