from flask import Blueprint,render_template

__all__ = ['bp']

bp = Blueprint('views.tasks',__name__,url_prefix='/')

@bp.route('/')
def index():
    return render_template('tasks.html')
