const completedElement = document.getElementById("completedTasks");
const pendingElement = document.getElementById("pendingTasks");
let tasks = {};

class Tasks extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            "type": props.type,
            "error": null,
            "loaded": false,
            "items": []
        };
        tasks[props.type] = this;
    }

    componentDidMount() {
        this.getTasks();
    }

    getTasks() {
        fetch("/api/tasks/" + this.state.type)
            .then(res => res.json())
            .then(
                (res) => {
                    this.setState({
                        "loaded": true,
                        "items": res
                    });
                },
                (err) => {
                    this.setState({
                        "loaded": true,
                        "error": err
                    });
                }
            );
    }

    render() {
        const {error, loaded, items, type} = this.state;
        const isPending = type == "pending";
        if (error)
            return <div>A error ocurr: {error.message}</div>;
        if (!loaded)
            return <div>Loading {type} tasks</div>;
        return (
            <div className="list-group">
              {items.map(i => (
                  <li
                    className={"list-group-item list-group-item-action btn " + (!isPending ? "disabled" : "")}
                    onClick={() => {
                        if (this.state.type == "pending")
                            completeTask(this, i.id);
                    }}>{i.text}</li>
              ))}
            </div>
        );
    }
}

function App(state) {
    return <Tasks type={state.type} />;
}

function completeTask(self, id) {
    fetch("/api/tasks/" + id + "/complete", {"method": "PUT"})
        .then(
            (res) => {
                if ([204, 409].indexOf(res.status) != -1)
                    return updateTasks();
                return self.setState({
                    "loaded": true,
                    "error": {
                        "message": "API returned status code " + res.status
                    }
                });
            },
            (err) => {
                self.setState({
                    "loaded": true,
                    "error": err
                });
            }
        );
}

function newTask() {
    let text = document.getElementById("taskText").value;
    if (text.length == 0)
        return alert("Task text must not be empty");
    let btn = document.getElementById("taskCreateBtn");
    btn.classList.toggle('disabled');
    fetch("/api/tasks/", {
        "method": "POST",
        "body": JSON.stringify({
            "text": text
        })
    }).then(
        (res) => {
            if (res.status == 201) {
                updateTasks();
                return btn.classList.toggle('disabled');
            }
            return self.setState({
                "loaded": true,
                "error": {
                    "message": "API returned status code " + res.status
                }
            });
        },
        (err) => {
            btn.classList.toggle('disabled');
            self.setState({
                "loaded": true,
                "error": err
            });
        }
    );
}

function updateTasks() {
    return Object.values(tasks).forEach(function (t) {
        t.getTasks();
    });
}

ReactDOM.render(<App type="completed" />, completedElement);
ReactDOM.render(<App type="pending" />, pendingElement);
