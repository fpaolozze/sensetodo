from simplejson import loads

__all__ = ['Config']

class Config(object):
    __settings = None

    def __init__(self):
        with open('settings.json') as f:
            self.__settings = loads(f.read())

    def __getitem__(self,key):
        return self.__settings[key]

Config = Config()
